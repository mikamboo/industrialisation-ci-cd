import { Component } from '@angular/core';
import { version } from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'IT Akademy';
  public version: string = this.getVersion(version);

  public getVersion(name) {
    if (name === '' || name === undefined){
      return '1.0.0-pre';
    }
    return this.version;
  }
}
